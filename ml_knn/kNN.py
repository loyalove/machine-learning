import numpy
import collections


def create_data_set():
    group = numpy.array([[1.0, 1.1], [1.0, 1.0], [0, 0], [0, 0.1]])
    labels = ['A', 'A', 'B', 'B']
    return group, labels


def classify0(inX, dataSet, labels, k):
    # 获取dataSet维度1的数据数量
    dataSetSize = dataSet.shape[0]
    # 构建与dataSet维度相同的数组
    exArr = numpy.tile(inX, (dataSetSize, 1))
    # 计算出dataSet每个点于inX的差值数组
    diffMat = exArr - dataSet
    # 差值平方
    sqDiffMat = diffMat ** 2
    # 计算与点的距离平方
    sqDistances = sqDiffMat.sum(axis=1)
    # 计算与点的距离
    distances = sqDistances ** 0.5
    # 距离排序
    sortedDistances = distances.argsort()
    # 保存距离前k个的labels
    classLabels = []
    for i in range(k):
        classLabels.append(labels[sortedDistances[i]])
    # 获取出现次数最多的label返回
    return collections.Counter(classLabels).most_common(1)[0][0]


group1, labels1 = create_data_set()

testPoint = [0, 0]
print(classify0(testPoint, group1, labels1, 4))